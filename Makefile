#
# Makefile for use with local development and CI builds
#


.PHONY: build clean down test up

all: build

build:
	docker-compose build

clean:
	docker system prune --force

down:
	docker-compose down

test: build
	rm -rf coverage
	mkdir -m755 coverage
	/usr/bin/env bash -c "\
		trap '${MAKE} down_ci' ERR EXIT SIGHUP SIGINT SIGKILL SIGQUIT SIGTERM;\
		docker-compose run --rm\
		-v ${CURDIR}/src:/src\
		-v ${CURDIR}/coverage:/coverage\
		-v ${CURDIR}/tests:/tests api\
		/bin/sh -c 'pytest --junitxml=/coverage/report.xml\
			--cov=/src/app --cov-report term-missing\
			--cov-report html:/coverage /tests -p no:cacheprovider'\
	"

test_ci: build
	rm -rf coverage
	mkdir -m777 coverage
	/usr/bin/env bash -c "\
		trap '${MAKE} down_ci' ERR EXIT SIGHUP SIGINT SIGKILL SIGQUIT SIGTERM;\
		docker-compose run --rm\
		-v ${CURDIR}/src:/src\
		-v ${CURDIR}/coverage:/coverage\
		-v ${CURDIR}/tests:/tests api\
		/bin/sh -c 'pytest --junitxml=/coverage/report.xml --cov=/src/app\
			--cov-report term-missing\
			--cov-report html:/coverage /tests -p no:cacheprovider'\
	"

up: build
	docker-compose up

# Flask API

File upload flask API.

The master branch contains a flask API to implement base functionality provided for
the rapport test task.

This hooks up the API via a custom basic login form. A session token is returned upon login for use with the
/upload and /files endpoints. Configure the required env vars in .env-sample to integrate with
database. Checkout the aws branch for integration with AWS Cognito User pool.

The following endpoints are provided:
- /login: [GET] Redirects to login form and returns a session token upon sign in for use with protected endpoints.
- /upload: [GET] Returns a HTML form for uploading a json file.
- /upload: [POST] Accepts a JSON payload for storing in database.
- /files: [GET] Returns a list of files formatted according to JSONAPI specification.
- /logout: [GET] Logout user.

## Technologies

It demonstrates the concepts of using the following practices and technologies:

- Using a factory method to create the Flask API server for increased testability.
- Flask blueprints to facilitate managing complexity of API endpoints.
- Marshmallow-jsonapi to generate JSON from an API resource that is compliance with JSONAPI specification.
- Marshmallow to validate json attributes.
- SQLAlchemy ORM to represent resource models for underlying DB.
- flask-sqlalchemy-session to provide a SQLALchemy session per API request.
- Some Swagger documentation created using flassger.
- Uwsgi WSGI server.
- Pytest for implementing and running unit and functional tests.
- Factory_boy library for generating fake test data for functional tests.
- Dependencies are managed using poetry.
- Docker file with multi-stage builds for building deps, development and production.


## Requirements specification

Interacts with a Database and supports user authentication.
The api should look like this:
## api.com/upload
[GET] [Authenticated user]
- User must be authenticated
- A form will be presented where user can select a file from the local computer to upload
[POST] [Authenticated user]
- User must be authenticated
- On valid file, upload the contents over the database
- Request will contain a Json as the payload body. If session is valid. The Json will be written in a database.
## api.com/
[GET] [No authenticated user]
- Present a form with `username` `password`.
[POST] [No authenticated user]
- Will fail if credentials are not present/incorrect
- On success a session token will be returned for onward authentication
[GET] [Authenticated user]
- List the database items


## Quickstart

Clone the repository and create a new python virtual environment. From the
project root folder issue the following commands. 

```bash
# install poetry dependency manager if not already installed
curl -sSL https://raw.githubusercontent.com/python-poetry/poetry/master/get-poetry.py | python -

# get poetry to activate a virtual environment
$ poetry shell

# Install development+production dependencies listed in pyproject.toml
# If a virtualenv is active poetry will use that, if not it will create a new one
# For further info https://python-poetry.org/docs/managing-environments/
$ poetry install

# run pre-commit install to install githooks for flake8 and black linting
pre-commit install

# if populated repository then it run hooks for all files
# pre-commit only runs for changed files by default
pre-commit run --all-files

# Docker accepts config from env vars, copy the sample and modify
# accordingly
cp .env-sample .env

# use the make script provided or docker-compose
$ make up

# OR build the postgres database and api docker images
$ docker-compose build

# start database and api docker containers
# the api container will mount src and test folders
# pytest-watch runs with pytest-testmon to check for changes
# in src and test folders re-running tests
$ docker-compose up

# when finished tear down docker-compose stack using make or docker-compose
$ make down
$ docker-compose down
```

If wish to run flask server on localhost then...

```console
$ cd src
$ FLASK_APP=app flask run
```

Visit index page at http://localhost:${API_PORT}/
Visit sample Swagger documentation at http://localhost:${API_PORT}/apidocs


## Docker

Currently a postgreSQL database is provided within a docker-compose environment.
This is used to create a database with empty tables. The following environment
variables can be used to configure the creation process of the database:

- POSTGRES_DB: The name of the database
- POTGRES_USER: The postgres user that is the owner of the database
- POSTGRES_PASSWORD: The database password for the owner of the database
- POSTGRES_SCHEMA: The name of the schema in the postgres database

On initial usage the database can be built using ```docker-compose build```

A docker container exists for the flask api in the docker-compose stack.
The docker-compose stack runs the development build target which mounts
the src and runs flask development server.

A production build target is also provided that copies the app source code
and runs a uwsgi on port 8081.


## Testing

Pytest has been used to implement unit and integration tests.
Tests exist within the *tests* folder of the project, with fixtures provided
*conftest.py*. The following test fixtures are provided:

- app: Creates a flask application wihin an app context.
- init_database: Populate database with fake test data using the *factory_boy* library.
Tests run and after completed, table rows are deleted. 

Run the tests within docker:

```console
make test
```

## CHANGELOG

Release - unreleased

### Added
- Flask-SQLAlchemy-Session used to provide ORM session on app context per request
- Marshmallow-JSON API to serialize/deserialize API responses according to JSONAPI spec
- Marshmallow-SQLAlchemy to output serialize/deserialize models to JSON
- Scripts for PostgreSQL docker container
- DB_SCHEMA stored in config for model
- Added Swagger doc endpoint, visit at http://localhost:${API_PORT}/apidocs
- Added swagger documentation for /files endpoint
- Added sample tests
- Added pyproject.toml with poetry dev and production dependencies
- Added pre-commit and associated hooks for running flake8 and black.


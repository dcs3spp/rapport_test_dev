#!/bin/bash

set -e
set -u

DB_APP_USER=${POSTGRES_USER}
DB_NAME=${POSTGRES_DB}
DB_SCHEMA=${POSTGRES_SCHEMA}

echo
echo "*******************************************"
echo "* REST-API DATABASE INSTALLATION SCRIPT   *"
echo "*******************************************"
echo
echo

psql \
  -U $DB_APP_USER \
  -f /docker-entrypoint-initdb.d/10-init-schemas.schemas \
  --echo-all \
  --set ON_ERROR_STOP=on \
  --set DB_NAME=$DB_NAME \
  --set DB_APP_USER=$DB_APP_USER \
  --set DB_SCHEMA=$DB_SCHEMA \
  -d $DB_NAME

if [ $? != 0 ]; then
  echo
  echo
  echo "psql failed while trying to run 10-init-schemas.schemas" 1>&2
  exit -2
fi
echo
echo
echo "10-init-schemas.schemas"

psql \
  -U $DB_APP_USER \
  -f /docker-entrypoint-initdb.d/11-init-extensions.ext \
  --echo-all \
  --set ON_ERROR_STOP=on \
  --set DB_NAME=$DB_NAME \
  --set DB_APP_USER=$DB_APP_USER \
  --set DB_SCHEMA=$DB_SCHEMA \
  -d $DB_NAME

if [ $? != 0 ]; then
  echo
  echo
  echo "psql failed while trying to run 11-init-extensions.ext" 1>&2
  exit -2
fi
echo
echo
echo "11-init-extensions.ext"

psql \
	-U $DB_APP_USER \
	-f /docker-entrypoint-initdb.d/12-init-tables.tables \
	--echo-all \
	--set ON_ERROR_STOP=on \
  --set DB_NAME=$DB_NAME \
  --set DB_APP_USER=$DB_APP_USER \
  --set DB_SCHEMA=$DB_SCHEMA \
	-d $DB_NAME

if [ $? != 0 ]; then
  echo 
  echo
  echo "psql failed while trying to run 12-init-tables.tables" 1>&2
  exit -2
fi
echo
echo
echo "12-init-tables.tables"
echo
echo
echo "install.sh script successful"
echo
echo
echo "*******************************************"
echo "* DATABASE INSTALLATION COMPLETE *"
echo "*******************************************"
echo
echo
echo


import os
from logging.config import dictConfig

from flasgger import Swagger
from flask import Flask
from flask_sqlalchemy_session import flask_scoped_session

from config import Configs

from .database import get_engine, get_session_factory
from .oidc import init_oidc_app

dictConfig(
    {
        "version": 1,
        "formatters": {
            "default": {
                "format": "[%(asctime)s] %(levelname)s in %(module)s: %(message)s",
            }
        },
        "handlers": {
            "wsgi": {
                "class": "logging.StreamHandler",
                "stream": "ext://flask.logging.wsgi_errors_stream",
                "formatter": "default",
            }
        },
        "root": {"level": "INFO", "handlers": ["wsgi"]},
    }
)


def _getConfig():
    """
    Get Config object from FLASK_ENV
    If FLASK_ENV is not set defaults to 'default'
    This is used to retrieve default config object
    in config.Configs.
    """

    flaskEnv = os.environ.get("FLASK_ENV")

    return Configs.get(flaskEnv, Configs["default"])


def create_app():
    """
    Factory method to create the flask app
    """

    app = Flask(__name__)
    app.config.from_object(_getConfig())

    # configure the db session
    settings = {"sqlalchemy.url": app.config["SQLALCHEMY_DATABASE_URI"]}

    engine = get_engine(settings)
    session = get_session_factory(engine)

    dbSession = flask_scoped_session(session)
    dbSession.init_app(app)

    init_oidc_app(app)

    Swagger(app)

    with app.app_context():
        from app.errors import errors_bp
        from app.views import file

        app.secret_key = app.config["SECRET_KEY"]
        app.register_blueprint(errors_bp)
        app.register_blueprint(file.files_bp)

    return app

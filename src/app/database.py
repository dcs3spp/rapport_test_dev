from sqlalchemy import engine_from_config
from sqlalchemy.orm import sessionmaker


def get_engine(settings, prefix="sqlalchemy."):
    """get_engine returns the engine from config dict"""
    return engine_from_config(settings, prefix)


def get_session_factory(engine):
    """get_session_factory creates a session factory that will bind to
    engine
    """
    factory = sessionmaker()
    factory.configure(bind=engine)
    return factory

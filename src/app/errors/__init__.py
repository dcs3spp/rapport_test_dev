"""API error type and handler"""
from flask import Blueprint, jsonify, make_response

errors_bp = Blueprint("errors_bp", __name__)


class APIError(ValueError):
    """Raised when server encountered an error processing a request."""

    default_pointer = "/data"

    def __init__(self, status, title, detail, pointer=None):
        self.pointer = pointer or self.default_pointer
        self.status = status
        self.title = title
        self.detail = detail

        super().__init__(self.detail)

    @property
    def messages(self):
        """JSON API-formatted error representation."""
        return {
            "errors": [
                {
                    "status": self.status,
                    "title": self.title,
                    "detail": self.detail,
                    "source": {"pointer": self.pointer},
                }
            ]
        }


@errors_bp.app_errorhandler(APIError)
def handle_error(error):
    """Returns a JSON API response for an APIError"""
    return make_response(jsonify(error.messages), error.status)

from sqlalchemy.orm import configure_mappers

from .file import FileModel  # noqa: F401

configure_mappers()

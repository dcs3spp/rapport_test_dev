import uuid

import psycopg2.extras
import sqlalchemy.dialects.postgresql
from sqlalchemy import Column, text
from sqlalchemy.dialects.postgresql import JSON
from sqlalchemy.types import String, TypeDecorator, TypeEngine

from .meta import Base

# Required for PostgreSQL to accept UUID type.
psycopg2.extras.register_uuid()


class UUID(TypeDecorator):
    """Converts UUID to string before storing to database.
    Converts string to UUID when retrieving from database.

    This class is database agnostic and is taken from
    https://gist.github.com/jmatthias/9262225. Code
    coverage has been excluded.
    """

    impl = TypeEngine

    def load_dialect_impl(self, dialect):  # pragma: no cover
        """When using Postgres database, use the Postgres UUID column type.
        Otherwise, use String column type."""
        if dialect.name == "postgresql":
            return dialect.type_descriptor(sqlalchemy.dialects.postgresql.UUID)

        return dialect.type_descriptor(String)

    def process_bind_param(self, value, dialect):  # pragma: no cover
        """When using Postgres database, no conversion.
        Otherwise, convert to string before storing to database."""
        if dialect.name == "postgres":
            return value

        if value is None:
            return value

        return str(value)

    def process_result_value(self, value, dialect):  # pragma: no cover
        """When using Postgres database, no conversion.
        Otherwise, convert to UUID when retrieving from database."""
        if dialect.name == "postgresql":
            return value

        if value is None:
            return value

        return uuid.UUID(value)


class FileModel(Base):
    __tablename__ = "file"

    id = Column(
        "FileID", UUID, primary_key=True, server_default=text("uuid_generate_v4()")
    )
    Filepath = Column("Filepath", String(500))
    Content = Column("Content", JSON, default=lambda: {})
    UserID = Column("UserID", String(10))

    def __init__(self, content, file_path, user_id):
        self.Content = content
        self.Filepath = file_path
        self.UserID = user_id

    def __eq__(self, other):
        return type(self) is type(other) and self.id == other.id

    def __ne__(self, other):
        return not self.__eq__(other)

    def __repr__(self):
        return "FileModel('%s','%s','%s')" % (self.id, self.Filepath, self.UserID)

    def __str__(self):
        return "('%s','%s','%s')" % (self.id, self.Filepath, self.UserID)

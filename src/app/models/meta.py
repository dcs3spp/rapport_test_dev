from flask import current_app

# This module exposes a Base property for use with SQLAlchemy models
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.schema import MetaData

# Recommended naming convention used by Alembic, as various different database
# providers will autogenerate vastly different names making migrations more
# difficult. See: http://alembic.zzzcomputing.com/en/latest/naming.html
NAMING_CONVENTION = {
    "ix": "ix_%(column_0_label)s",
    "uq": "uq_%(table_name)s_%(column_0_name)s",
    "ck": "ck_%(table_name)s_%(constraint_name)s",
    "fk": "fk_%(table_name)s_%(column_0_name)s_%(referred_table_name)s",
    "pk": "pk_%(table_name)s",
}

# Specify schema name to use
SCHEMA_NAME = current_app.config["DB_SCHEMA"]

metadata = MetaData(schema=SCHEMA_NAME, naming_convention=NAMING_CONVENTION)
Base = declarative_base(metadata=metadata)

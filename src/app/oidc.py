from flask_oidc import OpenIDConnect

oidc = OpenIDConnect()


def init_oidc_app(app):
    """Initilise the flask application for oidc use."""
    oidc.init_app(app)

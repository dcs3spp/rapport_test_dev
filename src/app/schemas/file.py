from marshmallow import validate
from marshmallow_jsonapi import Schema, fields

from app.models import FileModel


class FileSchema(Schema):
    """
    FileSchema is a class that is a JSON representation of
    a file upload. It contains the following attributes:

    id: UUID4 generated id
    Content: File content
    FilePath: The path to the file

    The filepath has been included for extensibility.
    In future release the content would not be returned in the JSON
    response payload. Instead the filepath would reference a link
    to S3 storage.
    """

    id = fields.Str(data_key="id", required=False)
    Content = fields.Str(
        required=True,
        data_key="file",
        validate=validate.Length(min=1, error="file length should be at least 1"),
        error_messages={"required": "file content required"},
    )
    FilePath = fields.Str(required=False, data_key="file_path")

    class Meta:
        model = FileModel
        ordered = True
        type_ = "files"
        self_url = "/files/{id}"
        self_url_kwargs = {"id": "<id>"}
        self_url_many = "/files/"
        strict = True

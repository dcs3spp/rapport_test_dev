"use strict";

let form = document.querySelector("#upload-form");

/*
 * Click handler for clear button
 * Resets pre element contents and file input
 */
document.querySelector("#clear-button").addEventListener("click", function () {
  form.reset();

  let text = document.querySelector("#file-contents");
  text.textContent = "";

  setButtonsState();
});

/**
 * Change handler triggered when file input is changed.
 */
document
  .querySelector("#file-input")
  .addEventListener("change", function (event) {
    var file = event.target.files[0];

    displayFile(file);
  });

/**
 * Set the disabled state for cancel and submit buttons
 * Disable buttons when there is no text content in file-contents element
 */
function setButtonsState() {
  let textArea = document.querySelector("#file-contents");
  let btnSubmit = document.querySelector("#submit-button");
  let btnCancel = document.querySelector("#clear-button");

  if (textArea.textContent.length > 0) {
    btnSubmit.disabled = false;
    btnCancel.disabled = false;
  } else {
    btnCancel.disabled = true;
    btnSubmit.disabled = true;
  }
}

/**
 * Display the contents of the input file selected.
 * The file contents are displayed in the pre element #file-contents.
 * @param file  File input in DOM
 */
function displayFile(file) {
  let reader = new FileReader();

  reader.addEventListener("load", function (e) {
    let text = e.target.result;
    document.querySelector("#file-contents").textContent = text;
    setButtonsState();
  });

  reader.readAsText(file);
}

/**
 * POST a json payload to /upload endpoint
 *
 * Use fetch to make the request.
 *
 * @param {string} json Json payload
 * @returns JSON response
 */
const sendJSON = async (json) => {
  const headers = {
    "Content-Type": "application/json",
  };

  const response = await fetch("/upload", {
    headers,
    method: "POST",
    body: json,
  });

  if (!response.ok) {
    throw new Error(`Sending file caused an error - ${response.statusText}`);
  }

  return response.json();
};

/**
 * Event handler for form submission
 */

const formUpload = document.querySelector("#upload-form");

document
  .querySelector("#upload-form")
  .addEventListener("submit", async (event) => {
    console.log("FORM SUBMIT HANDLER");

    event.preventDefault();

    let txtArea = document.querySelector("#file-contents");

    try {
      await sendJSON(txtArea.textContent);
      $(".modal-body").text("File upload was successful!");
    } catch (err) {
      $(".modal-body").text(err);
    }

    $("#resultModal").modal("toggle");
  });

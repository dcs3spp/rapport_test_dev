from flasgger import swag_from
from flask import (
    Blueprint,
    current_app,
    jsonify,
    redirect,
    render_template,
    request,
    session,
    url_for,
)
from flask_sqlalchemy_session import current_session
from sqlalchemy.exc import DBAPIError

from app.errors import APIError
from app.models import file
from app.schemas import file as file_schema

# from app.oidc import oidc

files_bp = Blueprint("files_bp", __name__)
user = {"username": "test", "password": "test"}


def JsonAPI(*args, **kwargs):
    """Wrapper around jsonify that sets the Content-Type of the response to
    application/vnd.api+json.
    """
    response = jsonify(*args, **kwargs)
    response.mimetype = "application/vnd.api+json"
    return response


@files_bp.route("/upload", methods=["POST"])
def store_file():
    """[POST] [Authenticated user]
    - User must be authenticated
    - On valid file, upload the contents over the database
    - Request will contain Json as the payload body. If session is valid.
    - The Json will be written in a database.
    """
    if "user" not in session:
        return redirect("/login")

    if not request.is_json:
        raise APIError(400, "Bad Request", "Expected json payload")

    content = request.get_json(force=True, silent=True)
    if content is None:
        raise APIError(400, "Bad Request", "Expected valid json payload")

    try:
        item = file.FileModel(
            content=content,
            file_path="json_file_path.json",
            user_id=session["user"],
        )

        current_session.add(item)
        current_session.commit()

        return jsonify(success=True)
    except DBAPIError:
        current_app.logger.exception("Database operational error encountered on upload")
        raise APIError(
            409,
            "Bad Request",
            "Database operational error encountered on upload",
        )
    except Exception as e:
        current_app.logger.exception(
            "Unexpected exception encountered while uploading file"
        )
        raise APIError(500, "Internal Server Error", repr(e))


@files_bp.route("/upload", methods=["GET"])
def upload_file():
    """[GET] [Authenticated user]
    - User must be authenticated
    - A form will be presented where user can select a file from the local computer to
      upload
    """
    if "user" not in session:
        return redirect("/login")

    return render_template("upload.html")


@files_bp.route("/files")
@swag_from("file.yml")
def list_files():
    """[GET] [Authenticated user]
    - List the files for authenticated user
    - TODO: Implement paging here
    """
    if "user" not in session:
        return redirect("/login")

    files = current_session.query(file.FileModel).filter(
        file.FileModel.UserID == session["user"]
    )

    data = file_schema.FileSchema(many=True).dump(files)
    return JsonAPI(data)

    # return render_template("files.html", user=session["user"], file_list=files)


@files_bp.route("/")
def index():
    """[GET] Render index page."""
    return render_template("index.html")


@files_bp.route("/login", methods=["POST", "GET"])
# @oidc.require_login
def login():
    """[GET] [No authenticated user]
    - Present a form with `username` `password`.
    [POST] [No authenticated user]
    - Will fail if credentials are not present/incorrect
    - On success a session token will be returned for onward authentication

    Redirects to list files
    """
    if request.method == "POST":
        username = request.form.get("username")
        password = request.form.get("password")
        if username == user["username"] and password == user["password"]:

            session["user"] = username
            return redirect("/upload")

        return (  # if the username or password does not matches
            "<h1>Wrong username or password</h1>"
        )

    return render_template("login.html")


@files_bp.route("/logout")
def logout():
    # oidc.logout()
    session.pop("user")
    return redirect(url_for(".index"))

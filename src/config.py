import os

app_dir = os.path.abspath(os.path.dirname(__file__))


class BaseConfig:
    OIDC_CLIENT_SECRETS = "./client_secrets.json"
    OIDC_COOKIE_SECURE = False
    OIDC_CALLBACK_ROUTE = "/oidc_callback"
    OIDC_SCOPES = ["openid", "email", "profile"]
    # OIDC_INTROSPECTION_AUTH_METHOD = "client_secret_post"
    OIDC_ID_TOKEN_COOKIE_NAME = "oidc_token"
    SECRET_KEY = os.environ.get("SECRET_KEY") or "secret"

    SQLALCHEMY_TRACK_MODIFICATIONS = False
    SWAGGER = {
        "title": "Users API",
        "uiversion": 3,
        "description": "API for managing user resources",
        "contact": {
            "responsibleOrganization": "ACME",
            "responsibleDeveloper": "Acme",
            "email": "acme@acme.com",
            "url": "www.example.com",
            "version": "0.0.1",
        },
    }


class DevelopmentConfig(BaseConfig):
    DB_SCHEMA = os.environ.get("DB_SCHEMA") or "schema"
    SQLALCHEMY_DATABASE_URI = (
        os.environ.get("DEVELOPMENT_DATABASE_URI")
        or "postgresql://docker:docker@localhost:5431/flask_app_db"
    )


class TestingConfig(BaseConfig):
    DB_SCHEMA = os.environ.get("DB_SCHEMA") or "schema"
    SQLALCHEMY_DATABASE_URI = (
        os.environ.get("TESTING_DATABASE_URI")
        or "postgresql://docker:docker@localhost:5431/flask_app_db"
    )
    TESTING = True


class ProductionConfig(BaseConfig):
    DB_SCHEMA = os.environ.get("DB_SCHEMA") or "schema"
    SQLALCHEMY_DATABASE_URI = (
        os.environ.get("PRODUCTION_DATABASE_URI")
        or "postgresql://docker:docker@localhost:5431/flask_app_db"
    )


Configs = {
    "development": DevelopmentConfig,
    "production": ProductionConfig,
    "testing": TestingConfig,
    "default": DevelopmentConfig,
}

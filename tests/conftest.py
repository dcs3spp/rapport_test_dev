# import contextlib

import pytest

from app import create_app

# from flask_sqlalchemy_session import current_session


@pytest.fixture
def app():
    app = create_app()
    return app


@pytest.fixture(scope="function")
def init_database(app):
    pass

    # Populate the tables with test data
    # with app.app_context():
    #    from tests.utils.factories import UserFactory

    # populate the tables in the database
    #    users = UserFactory.build_batch(10)
    #    current_session.add_all(users)
    #    current_session.commit()

    #    yield  # handover to testing!

    # after tests have run start clearing the tables
    #    with contextlib.closing(current_session.get_bind().connect()) as con:
    #        trans = con.begin()
    #        from app.models.meta import metadata

    #        for table in reversed(metadata.sorted_tables):
    #            con.execute(table.delete())
    #        trans.commit()

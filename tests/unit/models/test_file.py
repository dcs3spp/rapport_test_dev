import uuid

import pytest


@pytest.fixture
def file_model():
    from app.models import FileModel

    model = FileModel(content='{"content:" "json"}', file_path="json.json", user_id=2)
    model.id = str(uuid.uuid4())

    return model


def test_eq_returns_true_for_equal_instances(app, file_model):
    from app.models import FileModel

    other = FileModel(content='{"name": "ted"}', file_path="file.json", user_id=2)
    other.id = file_model.id

    assert file_model.id == other.id


def test_eq_returns_false_for_unequal_instances(app, file_model):
    from app.models import FileModel

    other = FileModel(content='{"name": "ted"}', file_path="file.json", user_id=2)
    other.id = str(uuid.uuid4())

    print("file_model.id := " + str(file_model.id))
    print("other.id := " + str(other.id))

    assert (file_model == other) is False


def test_ne_returns_true_for_unequal_instances(app, file_model):
    from app.models import FileModel

    other = FileModel(content='{"name": "bertha"}', file_path="file.json", user_id=2)
    other.id = str(uuid.uuid4())

    assert file_model != other


def test_ne_returns_false_for_equal_instances(app, file_model):
    from app.models import FileModel

    other = FileModel(content="{}", file_path="test.json", user_id=2)
    other.id = file_model.id

    assert (file_model != other) is False


def test_repr_returns_expected_string(app, file_model):

    expectedResult = "FileModel('%s','%s','%s')" % (
        file_model.id,
        file_model.Filepath,
        file_model.UserID,
    )

    result = repr(file_model)

    assert result == expectedResult


def test_str_returns_expected_string(app, file_model):

    expectedResult = "('%s','%s','%s')" % (
        file_model.id,
        file_model.Filepath,
        file_model.UserID,
    )

    result = str(file_model)

    assert result == expectedResult


def test_init_does_not_populate_id_property(app):
    from app.models import FileModel

    model = FileModel(content='{"name": "percival"}', file_path="json.json", user_id=2)

    assert model.id is None


def test_init_populates_file_path_property(app, file_model):

    expectedResult = "json.json"

    assert file_model.Filepath == expectedResult


def test_init_populates_content_property(app, file_model):

    expectedResult = '{"content:" "json"}'

    assert file_model.Content == expectedResult


def test_init_populates_userid_property(app, file_model):

    expectedResult = 2

    assert file_model.UserID == expectedResult
